const request = require('request');

const foreCast=((latitude,longitude,callback)=>{
    const url='http://api.weatherstack.com/current?access_key=621593a3b9af7a6d19ddfe47e60817e9&query='+latitude+','+longitude+'&units=f';
    request({url, json:true}, (error, response)=>{
        if(error){
            callback(`unable to connect to weather service`, undefined)
        }else if(response.body.error){
            callback('unable to find the location', undefined)

        } else{
            callback(undefined,response.body.current.weather_descriptions[0] +' It is currently '+response.body.current.temperature +' degree out. There is a '+response.body.current.precip+' % chance of rain.')
        }
    })
})
module.exports=foreCast;