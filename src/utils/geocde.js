const request= require('request');


// API Token For fetting geological location services
const geoCode= ((address,callback)=>{
    const url= 'https://api.mapbox.com/geocoding/v5/mapbox.places/'+encodeURIComponent(address)+'.json?access_token=pk.eyJ1IjoicmF2aTA4MDciLCJhIjoiY2s5amg2dmZ4MGZoMzNtbm8xcGJhaWh2ZyJ9.JKyHsEWe9qbAZbXYFNhyLQ&limit=1';
  
    request({url, json:true},(error,response)=>{
      if(error){
        callback("Unable to connect to geoservice", undefined)
      }else if(response.body.features.length===0){
        callback("Unable to find location try another search", undefined);
      }else{
        callback(undefined,{
           latitude:response.body.features[0].center[0],
           longitude: response.body.features[0].center[1],
           location:response.body.features[0].place_name
        })
      }
    })
  })


  module.exports=geoCode;